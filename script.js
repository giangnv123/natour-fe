"use strict";

/// Section
const allSections = document.querySelectorAll(".section");

// const revealSection = function (entries, observer) {
//   const [entry] = entries;
//   if (!entry.isIntersecting) return;
//   entry.target.classList.remove("section--hidden");
//   observer.unobserve(entry.target);
// };

// allSections.forEach((section) => section.classList.add("section--hidden"));

// const sectionObverser = new IntersectionObserver(revealSection, {
//   root: null,
//   threshold: 0.2,
// });

// allSections.forEach((section) => sectionObverser.observe(section));

const overlay = document.querySelector(".overlay");

///   show - close Slide
const slideContainer = document.querySelector(".slider__container");
const slideShowButton = document.querySelectorAll(".btn__slide");

const hideSlide = function () {
  slideContainer.classList.add("element--hidden");
  overlay.classList.add("hidden");
};

const showSlide = function (e) {
  slideContainer.classList.remove("element--hidden");
  overlay.classList.remove("hiden");
};

slideShowButton.forEach((btn) =>
  btn.addEventListener("click", function (e) {
    e.preventDefault();
    showSlide();
  })
);

/// Popup
const popup = document.querySelector(".popup");
const exitPop = document.querySelector(".exit");
const showPopNew = document.getElementsByClassName("show__pop");

const showPop = function () {
  popup.classList.remove("hiden");
  overlay.classList.remove("hiden");
  popup.style.transform = "scale(1)";
};

const hidePop = function () {
  popup.classList.add("hiden");
  overlay.classList.add("hiden");
  popup.style.transform = "scale(0.5)";
};

const showPopupBtn = document.querySelector(".popup__show");
showPopupBtn.addEventListener("click", function () {
  showPop();
});

exitPop.addEventListener("click", hidePop);
overlay.addEventListener("click", function () {
  hidePop(), hideSlide();
});

document.addEventListener("keydown", (e) => {
  if (e.key == "Escape") {
    hideSlide();
    hidePop();
  }
});

for (let i = 0; i < showPopNew.length; i++) {
  showPopNew[i].addEventListener("click", showPop);
}

//// Slider
const allSlides = document.querySelectorAll(".slide");

const dotsContainer = document.querySelector(".dots__container");
allSlides.forEach((_, i) => {
  const html = `<button class="dot__child" data-slide="${i}"></button>`;
  dotsContainer.insertAdjacentHTML("beforeend", html);
});

const updateDotUI = function (slide) {
  document.querySelectorAll(".dot__child").forEach((dot) => dot.classList.remove("dot__child--active"));
  document.querySelector(`button[data-slide="${slide}"]`).classList.add("dot__child--active");
};

dotsContainer.addEventListener("click", function (e) {
  if (e.target.classList.contains("dot__child")) {
    currentSlide = e.target.dataset.slide;
    goToSlide(currentSlide);
  }
});

var currentSlide = 0;

const goToSlide = function (slide) {
  allSlides.forEach((s, i) => (s.style.transform = `translateX(${(i - slide) * 100}%)`));
  updateDotUI(slide);
};

goToSlide(0);

const nextSlide = function () {
  currentSlide++;
  if (currentSlide === allSlides.length) currentSlide = 0;
  goToSlide(currentSlide);
};

const prevSlide = function () {
  currentSlide--;
  if (currentSlide < 0) currentSlide = allSlides.length - 1;
  goToSlide(currentSlide);
};

const nextSlideButton = document.querySelector(".slide__button--right");
const prevSlideButton = document.querySelector(".slide__button--left");

nextSlideButton.addEventListener("click", nextSlide);
prevSlideButton.addEventListener("click", prevSlide);

// slide by keyboard
document.addEventListener("keydown", function (e) {
  if (e.key === "ArrowRight") nextSlide();
  if (e.key === "ArrowLeft") prevSlide();
});
